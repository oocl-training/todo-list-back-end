package com.baldwin.todolist.controller;

import com.baldwin.todolist.model.TodoItem;
import com.baldwin.todolist.repository.TodoItemRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

@SpringBootTest
@AutoConfigureMockMvc
class TodoItemControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TodoItemRepository todoItemRepository;


    @BeforeEach
    void setUp() {
        todoItemRepository.deleteAll();
    }

    @Test
    void should_return_todoItem_when_addTodoItem() throws Exception {
        // given
        TodoItem todoItem = new TodoItem();
        todoItem.setText("complete");
        todoItem.setDone(false);
        String todoItemJson = new ObjectMapper().writeValueAsString(todoItem);
        // when
        ResultActions resultActions = mockMvc.perform(post("/todoItems").contentType(MediaType.APPLICATION_JSON)
                                                                        .content(todoItemJson));
        // then
        resultActions.andExpect(jsonPath("$.id").exists()).andExpect(jsonPath("$.text").value(todoItem.getText()))
                     .andExpect(jsonPath("$.done").value(todoItem.getDone()));
    }

    @Test
    void should_delete_todoItem_when_removeTodoItem() throws Exception {
        // given
        TodoItem todoItem = new TodoItem();
        todoItem.setText("content");
        todoItem.setDone(false);
        todoItemRepository.save(todoItem);
        // when
        mockMvc.perform(delete("/todoItems/{id}", todoItem.getId()));
        // then
        assertEquals(Optional.empty(), todoItemRepository.findById(todoItem.getId()));
    }

    @Test
    void should_return_todoItems_when_getTodoItems() throws Exception {
        // given
        TodoItem todoItem = new TodoItem();
        todoItem.setText("content");
        todoItem.setDone(false);
        todoItemRepository.save(todoItem);
        // when
        ResultActions resultActions = mockMvc.perform(get("/todoItems"));
        // then
        resultActions.andExpect(jsonPath("$[0].id").value(todoItem.getId()))
                     .andExpect(jsonPath("$[0].text").value(todoItem.getText()))
                     .andExpect(jsonPath("$[0].done").value(todoItem.getDone()));
    }

    @Test
    void should_return_todoItem_when_getTodoItemById() throws Exception {
        // given
        TodoItem todoItem = new TodoItem();
        todoItem.setText("content");
        todoItem.setDone(false);
        todoItemRepository.save(todoItem);
        // when
        ResultActions resultActions = mockMvc.perform(get("/todoItems/{id}", todoItem.getId()));
        // then
        resultActions.andExpect(jsonPath("$.id").value(todoItem.getId()))
                     .andExpect(jsonPath("$.text").value(todoItem.getText()))
                     .andExpect(jsonPath("$.done").value(todoItem.getDone()));
    }

    @Test
    void should_return_todoItem_when_updateTodoItem() throws Exception {
        // given
        TodoItem todoItemUpdateTarget = new TodoItem();
        todoItemUpdateTarget.setText("content");
        todoItemUpdateTarget.setDone(false);
        todoItemRepository.save(todoItemUpdateTarget);

        TodoItem todoItemUpdateSource = new TodoItem();
        todoItemUpdateSource.setText("English");
        todoItemUpdateSource.setDone(true);
        String todoItemJson = new ObjectMapper().writeValueAsString(todoItemUpdateSource);

        // when
        ResultActions resultActions =
                mockMvc.perform(put("/todoItems/{id}", todoItemUpdateTarget.getId()).contentType(MediaType.APPLICATION_JSON)
                                                                                    .content(todoItemJson));
        // then
        resultActions.andExpect(jsonPath("$.id").value(todoItemUpdateTarget.getId()))
                     .andExpect(jsonPath("$.text").value(todoItemUpdateSource.getText()))
                     .andExpect(jsonPath("$.done").value(todoItemUpdateSource.getDone()));
    }


}