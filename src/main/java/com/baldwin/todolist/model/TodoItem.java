package com.baldwin.todolist.model;


import lombok.Data;

import javax.persistence.*;

@Entity
@Data
@Table(name = "todo_items")
public class TodoItem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String text;
    private Boolean done;
}
