package com.baldwin.todolist.controller;


import com.baldwin.todolist.service.TodoItemService;
import com.baldwin.todolist.service.dto.TodoItemRequest;
import com.baldwin.todolist.service.dto.TodoItemResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/todoItems")
public class TodoItemController {
    private final TodoItemService todoItemService;

    public TodoItemController(TodoItemService todoItemService) {
        this.todoItemService = todoItemService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TodoItemResponse addTodoItem(@RequestBody TodoItemRequest todoItemRequest) {
        return todoItemService.addTodoItem(todoItemRequest);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void removeTodoItem(@PathVariable Long id) {
        todoItemService.removeTodoItem(id);
    }

    @GetMapping
    public List<TodoItemResponse> getTodoItems() {
        return todoItemService.getTodoItems();
    }

    @GetMapping("/{id}")
    public TodoItemResponse getTodoItemById(@PathVariable Long id) {
        return todoItemService.getTodoItemById(id);
    }

    @PutMapping("/{id}")
    public TodoItemResponse updateTodoItem(@PathVariable Long id, @RequestBody TodoItemRequest todoItemRequest) {
        return todoItemService.updateTodoItem(id, todoItemRequest);
    }


}
