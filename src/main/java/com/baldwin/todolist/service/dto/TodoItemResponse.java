package com.baldwin.todolist.service.dto;


import lombok.Data;

@Data
public class TodoItemResponse {
    private Long id;
    private String text;
    private Boolean done;
}
