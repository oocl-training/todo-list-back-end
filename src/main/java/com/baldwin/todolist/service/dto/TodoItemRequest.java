package com.baldwin.todolist.service.dto;


import lombok.Data;

@Data
public class TodoItemRequest {
    private String text;
    private Boolean done;
}
