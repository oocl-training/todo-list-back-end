package com.baldwin.todolist.service.mapper;

import com.baldwin.todolist.model.TodoItem;
import com.baldwin.todolist.service.dto.TodoItemRequest;
import com.baldwin.todolist.service.dto.TodoItemResponse;
import org.springframework.beans.BeanUtils;

public class TodoItemMapper {
    public static TodoItem toEntity(TodoItemRequest todoItemRequest) {
        TodoItem todoItem = new TodoItem();
        BeanUtils.copyProperties(todoItemRequest, todoItem);
        return todoItem;
    }

    public static TodoItemResponse toResponse(TodoItem todoItem) {
        TodoItemResponse todoItemResponse = new TodoItemResponse();
        BeanUtils.copyProperties(todoItem, todoItemResponse);
        return todoItemResponse;
    }
}
