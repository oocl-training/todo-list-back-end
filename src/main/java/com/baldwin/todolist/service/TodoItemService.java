package com.baldwin.todolist.service;

import com.baldwin.todolist.exception.ResourceNotFoundException;
import com.baldwin.todolist.model.TodoItem;
import com.baldwin.todolist.repository.TodoItemRepository;
import com.baldwin.todolist.service.dto.TodoItemRequest;
import com.baldwin.todolist.service.dto.TodoItemResponse;
import com.baldwin.todolist.service.mapper.TodoItemMapper;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class TodoItemService {

    private final TodoItemRepository todoItemRepository;

    public TodoItemService(TodoItemRepository todoItemRepository) {
        this.todoItemRepository = todoItemRepository;
    }

    public TodoItemResponse addTodoItem(TodoItemRequest todoItemRequest) {
        return TodoItemMapper.toResponse(todoItemRepository.save(TodoItemMapper.toEntity(todoItemRequest)));
    }

    public void removeTodoItem(Long id) {
        todoItemRepository.deleteById(id);
    }

    public List<TodoItemResponse> getTodoItems() {
        return todoItemRepository.findAll().stream().map(TodoItemMapper::toResponse).collect(Collectors.toList());
    }

    public TodoItemResponse getTodoItemById(Long id) {
        return TodoItemMapper.toResponse(todoItemRepository.findById(id).orElseThrow(ResourceNotFoundException::new));
    }


    public TodoItemResponse updateTodoItem(Long id, TodoItemRequest todoItemRequest) {
        TodoItem todoItem = TodoItemMapper.toEntity(todoItemRequest);
        if (todoItemRepository.findById(id).isEmpty()) {
            throw new ResourceNotFoundException();
        }
        todoItem.setId(id);
        todoItemRepository.save(todoItem);
        return TodoItemMapper.toResponse(todoItem);
    }

}
