create table if not exists todo_items
(
    id
    bigint
    not
    null
    auto_increment
    primary
    key,
    text
    varchar
(
    255
) not null,
    done boolean
    );