## ORID

---

### O (Objective)

`What did we learn today? What activities did you do? What scenes have impressed you?`

1. Integration between front and back end
2. Cross Domain
3. Two tools of Agile development including user journey and user story
4. Elevator speech
5. MVP
6. IPM
7. kick off
8. desk check

---

### R (Reflective)

`Please use one word to express your feelings about today's class.`

+ Tired

---

### I (Interpretive)

`What do you think about this? What was the most meaningful aspect of this activity?`

1. Integration between front and back end

2. Cross Domain

   Today I learned the reasons for cross domain issues. That's because the browser's homologous strategy. 

   If we want to access the back-end from front-end in browser, we need to obtain the backend's consent, which will response the body that allow access. As long as the backend agrees, the browser will be released. Certainly, we also can jump out of browser and access directly.

   There are two solutions, one is the front-end method and the other is the back-end method. The back-end mothod is just like content mentioned earlier. The front-end method is Utilizing network proxies.

3. Two tools of Agile development including user journey and user story

   We can use user journey to find users' problem and find out their code needs, and then summarize these into User story. These user stories can be broken down into smallest units that can be developed.

4. Elevator speech

   Utilize passion and product highlights to capture customers' hearts in the shortest possible time.

5. MVP

   The minimum deliverable and valuable product that can enhance users' confidence in continuing to invest


---

### D (Decisional)

`Where do you most want to apply what you have learned today? What changes will you make?`

+ Apply to the process of product development. Try to use these Agile method.